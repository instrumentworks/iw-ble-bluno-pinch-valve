/**
    Arduino  - BLE - Proportional Pinch Valve.
    Bluno Build.
    (c) 2015 Instrument Works Pty Ltd
    MIT License
    Version: 1.25
 */


#include <Bluno.h>

bool DEBUG = false;

int versionNumber = 125;

// Defines the pin connections from the chopper controller and 
int stepPin = 9;  // defines the pin which drives the stepper moter
int directionPin = 8;  // defines the pin which drives the direction of the stepper motor


// Initialises the variable for the current position of the valve 
int currentPosition;
int initial = 0; 
int totalSteps = 2400L;

// Indicates total number of steps for the full range of the valve
// This number is obtained from the settings on the stepper controller
int fullRange = 2400L;  

// Defines whether the valves default state is normall open (0), normally closed (1) or retains current position (2).


int spMax = 100;
int spMin = 0;
Bluno bluno(versionNumber,spMax,spMin,true);

bool newSetPoint(double sp){
  if (DEBUG == true){
    bluno.ReturnCommand("Set Position");
    
  }
  int movePosition = sp - currentPosition;
  if (movePosition < 0){
    stepperDirection("close");
  } else {
    stepperDirection("open");
  }
  movePosition = abs(movePosition);
  int numberOfSteps = map(movePosition, 0, 100, 0, (totalSteps-initial));
  stepperMove(numberOfSteps);
  currentPosition = sp;
  return true;
}
bool calibrate(String command){
  char instruction = command.charAt(1);
  if (instruction == 'I') {
    if (DEBUG == true)
      bluno.ReturnCommand("Set Startpoint");
    initialpoint(currentPosition);
  } else if (instruction == 'K') {
    if (DEBUG == true)
      bluno.ReturnCommand("Clear Startpoint");
    clearinitialpoint();
  } else if (instruction == 'E') {
    if (DEBUG == true)
      bluno.ReturnCommand("Set Endpoint");
    endpoint(currentPosition);
  } else if (instruction == 'C') {
    if (DEBUG == true)
      bluno.ReturnCommand("Clear Endpoint");
    clearendpoint();
  } else {
    bluno.PrintError();
    return false;
  }
  return true;
}
bool stopDevice(){
  return true;
}
double currentSetPoint(){
  return currentPosition;
}
bool customCommand(String command){
  char instruction = command.charAt(0);
  if (instruction == 'X') {
    command.remove(0,1);
    int debugState = command.toFloat();
    if (debugState == 1) {
      DEBUG = true;
    } else if (debugState == 0) {
      DEBUG = false;
    } else {
      bluno.PrintError();
    }    
    if (DEBUG == true){
      bluno.ReturnCommand("You have enabled debug mode");
    } else {
      bluno.ReturnCommand("You have disabled debug mode");
    }
  return true;
  } else {
    return false;
  }
}

void setup() {
  // put your setup code here, to run once:
  bluno.Setup(115200);  // open communication port to BLE chip
  pinMode(directionPin, OUTPUT);  // sets directionPin pin to output
  pinMode(stepPin, OUTPUT);  // sets stepPin pin to output
  digitalWrite(directionPin, LOW);  // initialises both directionPin and StepPin pins as low.
  digitalWrite(stepPin,LOW);
  stepperDirection("open");
  stepperMove(fullRange);
  currentPosition = 100;
}

void loop() {
    bluno.ReadCommand();
 } 
// Sets the new endpoint for the valve closed position. This function ensures the valve
// can't close too far and damage the tubing.
void endpoint(int settingPosition)  {
  int currentStepPosition = map(currentPosition, 100, 0, initial, totalSteps);
  if (DEBUG == true) {
    Serial.print("current step position ");
    Serial.println(currentStepPosition);
  }  
  totalSteps  = currentStepPosition;
    if (totalSteps <= initial) {
    bluno.ReturnCommand("End Point Error");
    return;
  }

  int newRangePosition = map(currentStepPosition, initial, totalSteps, 100, 0);
  currentPosition = newRangePosition;

  if (DEBUG == true) {
    Serial.print("total number of steps ");
    Serial.println(totalSteps);
    Serial.print("new current position ");
  }
  
}

// Sets the new startpoint for the valve opened position. This function ensures the valve
//locks the tubing in, and allows user to define the open position.
void initialpoint(int settingPosition)  {
  int currentStepPosition = map(currentPosition, 100, 0, initial, totalSteps);
  if (DEBUG == true) {
    Serial.print("current step position ");
    Serial.println(currentStepPosition);
  }  
  initial  = currentStepPosition;
    if (totalSteps <= initial) {
    Serial.print("Start Point Error");
    return;
  }
  int newRangePosition = map(currentStepPosition, initial, totalSteps, 100, 0);
  currentPosition = newRangePosition;
  if (DEBUG == true) {
    Serial.print("valve starting from step ");
    Serial.println(initial);
    Serial.print("new current position ");
  }
}

// Clears the endpoint setting to allow the full range of the valve to be used
void clearendpoint()  {
  int currentStepPosition = map(currentPosition, 100, 0, initial, totalSteps);
  int fullRangePosition = map(currentStepPosition, initial, fullRange, 100, 0);
  currentPosition = fullRangePosition;
  
  totalSteps = fullRange;
  
  if (DEBUG == true) {
    Serial.print("current step position: ");
    Serial.println(currentStepPosition);
    Serial.print("current position: ");
  }
}

// Clears the startpoint setting to allow the full range of the valve to be used
void clearinitialpoint()  {
  int currentStepPosition = map(currentPosition, 100, 0, initial, totalSteps);
  int fullRangePosition = map(currentStepPosition, 0, totalSteps, 100, 0);
  currentPosition = fullRangePosition;
  initial = 0;
  
  if (DEBUG == true) {
    Serial.print("current step position: ");
    Serial.println(currentStepPosition);
    Serial.print("current position: ");
  }

}

// Writes to the valve driver to tell it how far to open or close.
// steps = is the number of steps that it will operate.
void stepperMove(int steps){
  if (DEBUG == true) {
    Serial.print("moving steps: ");
    Serial.println(steps);
  }  
  for (int i=0; i <= steps; i++){
    digitalWrite(stepPin,HIGH);  // the next four lines form a pulse to drive the motor in the chosen direction
    delay(1);
    digitalWrite(stepPin,LOW);
    delay(1);
  }
}

// Writes the direction of the movement to the driver
// when low = closes the valve
// when high = opens the valve
void stepperDirection(String direct){
  if (direct == "close"){
    if (DEBUG == true) 
      Serial.println("closing valve");
    digitalWrite(directionPin, LOW);  
  } else if (direct == "open") {
      digitalWrite(directionPin, HIGH);
      if (DEBUG == true) 
        Serial.println("opening valve");
  } else {
      Serial.println("ER003");
        char waste[65] = {Serial.readBytes(waste,64)};
}}

