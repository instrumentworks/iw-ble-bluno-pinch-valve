#Bluno Pinch Valve#

This repository contains codes for controlling a miniature proportional pinch valve (MMPV-4) driven by an Advance Micro Systems mmd-17 chopper drive and a DFRobot Bluno unit. Operations of the pinch valve are controlled by user commands sent through BLE communication from the Dataworks App to the Bluno unit. Communication between the Bluno unit and the Dataworks App is handled by Bluno Arduino Library.

For full details on the valve, you can visit (insert link) for a blog on the valve.
For more information on the Bluno Arduino Library, you can visit the repository [here](https://bitbucket.org/instrumentworks/iw-bluno-library) for details on the functions provided by the library.

![valve nano prototype sketch_bb.png](https://bitbucket.org/repo/pxjgLj/images/3562152849-valve%20nano%20prototype%20sketch_bb.png)

#Actions and corresponding commands#

|**Action**        |**Command**  |**Comments**                              |
|:----------------:|:-----------:|:-----------------------------------------|
|Place new set point|S###|Where ### is a number from 0 to 100|
|Display current set point|S?|Returns the value of the current set point|
|Display set point range|SR|This should always be 0 to 100|
|Create new start location|CI|Sets the current position to be the new starting point|
|Create new end location|CE|Sets the current position to be the new end point|
|Clear start location|CK|Reset the starting point to default|
|Clear end location|CC|Reset the end point to default|
|Display Version Number|V|Version number #.## is displayed in integer form as ###|
|Enable Debug mode|X1|Enable debug mode to elaborate output messages|
|Disable Debug mode|X0|Disable debug mode to suppress output messages|